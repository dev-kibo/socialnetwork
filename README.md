# Diplomski rad na temu: Kolaborativna platforma za društvene aktivnosti za decu sa autizmom

## Opis tehnologija

- Projekat razvijen u [Angular](https://angular.io/) 13.3.3 verziji.
- Za stilizovanje korišćen je [tailwindcss](https://tailwindcss.com/).

### Pokretanje projekta

- Pokrenuti `ng serve` komandu za podizanje aplikacije.
- Otvoriti `http://localhost:4200/` u veb pregledaču.


#### Dodatne informacije

Projekat je prezentacionog tipa i koristi mock podatke.    
Strana za prijavu se nalazi na `http://localhost:4200/account/login`    
Strana za registraciju se nalazi na `http://localhost:4200/account/register`
