module.exports = {
  content: ["./src/**/*.{html,ts}"],
  theme: {
    maxWidth: {
      44: "11rem",
      64: "16rem"
    },
    extend: {
      transitionProperty: {
        width: "width",
        height: "height",
        border: "border",
        background:"background"
      },
      spacing: {
        18: "4.5rem",
        22: "5.5rem",
      },
    },
  },
  plugins: [],
};
