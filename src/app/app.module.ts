import { LOCALE_ID, NgModule } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeSr from '@angular/common/locales/sr';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { ChatComponent } from './chat/chat.component';
import { ChatContact } from './chat/chat-contact/chat-contact.component';
import { MessageWindowComponent } from './chat/message-window/message-window.component';
import { NewMessageWindowComponent } from './chat/new-message-window/new-message-window.component';
import { MessageDirective } from './chat/directives/message.directive';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import { NewPostComponent } from './home/new-post/new-post.component';
import { TextPostComponent } from './post/post.component';
import { CommentComponent } from './post/comment/comment.component';
import { NewCommentComponent } from './post/new-comment/new-comment.component';
import { PeopleComponent } from './people/people.component';
import { PersonCardComponent } from './people/person-card/person-card.component';
import { GroupsComponent } from './groups/groups.component';
import { GroupCardComponent } from './groups/group-card/group-card.component';
import { EventsComponent } from './events/events.component';
import { EventCardComponent } from './events/event-card/event-card.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { PostsComponent } from './user-profile/posts/posts.component';
import { InformationComponent } from './user-profile/information/information.component';
import { FriendsComponent } from './user-profile/friends/friends.component';
import { FriendComponent } from './user-profile/friends/friend/friend.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { NotificationComponent } from './notifications/notification/notification.component';
import { GroupComponent } from './group/group.component';
import { MembersComponent } from './group/members/members.component';
import { GroupPostsComponent } from './group/group-posts/group-posts.component';
import { MemberComponent } from './group/members/member/member.component';
import { EventComponent } from './event/event.component';
import { EventInformationComponent } from './event/event-information/event-information.component';
import { EventAttendeesComponent } from './event/event-attendees/event-attendees.component';
import { EventAttendeeComponent } from './event/event-attendees/event-attendee/event-attendee.component';
import { CreateGroupComponent } from './groups/create-group/create-group.component';
import { CreateEventComponent } from './events/create-event/create-event.component';
import { FriendSuggestionComponent } from './home/friend-suggestion/friend-suggestion.component';
import { ProfileFriendCardComponent } from './user-profile/profile-friend-card/profile-friend-card.component';
import { ProfileCommunityCardComponent } from './user-profile/profile-community-card/profile-community-card.component';
import { CommunitiesComponent } from './user-profile/communities/communities.component';
import { CommunityComponent } from './user-profile/communities/community/community.component';
import { ProfileComponent } from './profile/profile.component';
import { PrivateComponent } from './profile/private/private.component';
registerLocaleData(localeSr);
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ChatComponent,
    ChatContact,
    MessageWindowComponent,
    NewMessageWindowComponent,
    MessageDirective,
    HeaderComponent,
    MainComponent,
    NewPostComponent,
    TextPostComponent,
    CommentComponent,
    NewCommentComponent,
    PeopleComponent,
    PersonCardComponent,
    GroupsComponent,
    GroupCardComponent,
    EventsComponent,
    EventCardComponent,
    UserProfileComponent,
    PostsComponent,
    InformationComponent,
    FriendsComponent,
    FriendComponent,
    NotificationsComponent,
    NotificationComponent,
    GroupComponent,
    MembersComponent,
    GroupPostsComponent,
    MemberComponent,
    EventComponent,
    EventInformationComponent,
    EventAttendeesComponent,
    EventAttendeeComponent,
    CreateGroupComponent,
    CreateEventComponent,
    FriendSuggestionComponent,
    ProfileFriendCardComponent,
    ProfileCommunityCardComponent,
    CommunitiesComponent,
    CommunityComponent,
    ProfileComponent,
    PrivateComponent,
  ],
  imports: [BrowserModule, AppRoutingModule],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'sr-RS',
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
