import { Component, OnInit, ElementRef, HostListener } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { User } from '../chat/models/user.model';
import { ChatService } from '../chat/services/chat.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  isMenuVisible: boolean = false;
  isHeaderVisible: boolean = true;
  isProfileNavigated: boolean = false;
  searchResults: any[] = [];

  constructor(
    private elementRef: ElementRef,
    private router: Router,
    private chatService: ChatService
  ) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (event.url.startsWith('/profile/me')) {
          this.isProfileNavigated = true;
        } else {
          this.isProfileNavigated = false;
        }
      }
    });
  }

  ngOnInit(): void {}

  toggleMenu(): void {
    this.isMenuVisible = !this.isMenuVisible;
  }

  hasRoute(route: string): boolean {
    return this.router.url.includes(route);
  }

  signOut(): void {
    this.router.navigateByUrl('/account/login');
  }

  search(event: Event): void {
    const value = (event.target as HTMLInputElement).value;
    if (value.length > 0) {
      this.searchResults = this.chatService
        .searchUsers(value)
        .filter((x, index) => index < 10);
    } else {
      this.searchResults = [];
    }
  }

  resetSearch(): void {
    this.searchResults = [];
  }

  @HostListener('document:mousedown', ['$event'])
  onClick(event: MouseEvent): void {
    if (!this.elementRef.nativeElement.contains(event.target)) {
      this.isMenuVisible = false;
      this.searchResults = [];
    }
  }
}
