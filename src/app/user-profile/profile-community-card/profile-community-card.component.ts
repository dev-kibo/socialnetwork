import { Component, Input, OnInit } from '@angular/core';
import { Group } from 'src/app/groups/group.model';

@Component({
  selector: 'app-profile-community-card',
  templateUrl: './profile-community-card.component.html',
  styleUrls: ['./profile-community-card.component.css'],
})
export class ProfileCommunityCardComponent implements OnInit {
  @Input() group?: Group;
  constructor() {}

  ngOnInit(): void {}
}
