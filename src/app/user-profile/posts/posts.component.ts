import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../home/home.service';
import { Post } from '../../home/post.model';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css'],
})
export class PostsComponent implements OnInit {
  posts: Post[];
  constructor(private homeService: HomeService) {
    this.posts = this.homeService.getPosts();
  }

  ngOnInit(): void {}
}
