import { Component, OnInit } from '@angular/core';
import { ChatService } from '../../chat/services/chat.service';
import { User } from '../../chat/models/user.model';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css'],
})
export class FriendsComponent implements OnInit {
  friends: User[];
  constructor(private chatService: ChatService) {
    this.friends = this.chatService.getUsers().slice(0, 30);
  }

  ngOnInit(): void {}
}
