import { Component, Input, OnInit } from '@angular/core';
import { User } from '../../../chat/models/user.model';

@Component({
  selector: 'app-friend',
  templateUrl: './friend.component.html',
  styleUrls: ['./friend.component.css'],
})
export class FriendComponent implements OnInit {
  @Input() friend?: User;
  constructor() {}

  ngOnInit(): void {}
}
