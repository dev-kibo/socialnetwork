import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { ChatService } from '../chat/services/chat.service';
import { User } from '../chat/models/user.model';
import { Group } from '../groups/group.model';
import { GroupsService } from '../groups/groups.service';

@Component({
  selector: 'app-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
})
export class UserProfileComponent implements OnInit {
  friends: User[];
  groups: Group[];
  routerLinkActiveOptions = {
    exact: true,
  };

  showSideResults: boolean = true;

  constructor(
    private router: Router,
    private chatService: ChatService,
    private groupsService: GroupsService
  ) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (
          event.url === '/profile/me/friends' ||
          event.url === '/profile/me/communities'
        ) {
          this.showSideResults = false;
        } else {
          this.showSideResults = true;
        }
      }
    });
    this.friends = this.chatService.getUsers().slice(0, 10);
    this.groups = this.groupsService.getGroups().slice(0, 6);
  }

  ngOnInit(): void {}
}
