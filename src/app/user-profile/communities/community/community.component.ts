import { Component, Input, OnInit } from '@angular/core';
import { Group } from '../../../groups/group.model';

@Component({
  selector: 'app-community',
  templateUrl: './community.component.html',
  styleUrls: ['./community.component.css'],
})
export class CommunityComponent implements OnInit {
  @Input() group?: Group;
  constructor() {}

  ngOnInit(): void {}
}
