import { Component, OnInit } from '@angular/core';
import { GroupsService } from 'src/app/groups/groups.service';
import { Group } from '../../groups/group.model';

@Component({
  selector: 'app-communities',
  templateUrl: './communities.component.html',
  styleUrls: ['./communities.component.css'],
})
export class CommunitiesComponent implements OnInit {
  groups: Group[];
  constructor(private groupsService: GroupsService) {
    this.groups = this.groupsService.getGroups().slice(13);
  }

  ngOnInit(): void {}
}
