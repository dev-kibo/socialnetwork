import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class InformationService {
  private isModalOpenSource = new Subject<boolean>();
  constructor() {}

  changeModalState(state: boolean): void {
    this.isModalOpenSource.next(state);
  }

  get isModalOpen(): Observable<boolean> {
    return this.isModalOpenSource.asObservable();
  }
}
