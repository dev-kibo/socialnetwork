import { Component, OnInit } from '@angular/core';
import { InformationService } from './information.service';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css'],
})
export class InformationComponent implements OnInit {
  isEditModalOpen: boolean = false;

  constructor(private informationService: InformationService) {}

  ngOnInit(): void {}

  closeEditModal(): void {
    this.isEditModalOpen = false;
    this.informationService.changeModalState(this.isEditModalOpen);
  }

  openEditModal(): void {
    this.isEditModalOpen = true;
    this.informationService.changeModalState(this.isEditModalOpen);
  }
}
