import { Component, Input, OnInit } from '@angular/core';
import { User } from '../../chat/models/user.model';

@Component({
  selector: 'app-profile-friend-card',
  templateUrl: './profile-friend-card.component.html',
  styleUrls: ['./profile-friend-card.component.css'],
})
export class ProfileFriendCardComponent implements OnInit {
  @Input() friend?: User;
  constructor() {}

  ngOnInit(): void {}
}
