import { Component, ViewEncapsulation } from '@angular/core';
import { InformationService } from './user-profile/information/information.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  isInformationModalOpen: boolean = false;
  title = 'SocialNetwork';

  constructor(private infromationService: InformationService) {
    this.infromationService.isModalOpen.subscribe(
      (x) => (this.isInformationModalOpen = x)
    );
  }
}
