import { Component, OnInit } from '@angular/core';
import { Event } from './event.model';
import { EventsService } from './events.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css'],
})
export class EventsComponent implements OnInit {
  constructor(private eventsService: EventsService) {}

  ngOnInit(): void {}

  get events(): Event[] {
    return this.eventsService.getEvents();
  }
}
