import { Injectable } from '@angular/core';
import { EVENTS } from 'src/data/events.data';
import { Event } from './event.model';

@Injectable({
  providedIn: 'root',
})
export class EventsService {
  private events: Event[] = EVENTS;
  constructor() {}

  getEvents(): Event[] {
    return this.events;
  }
}
