export interface Event {
  id: number;
  name: string;
  location: string;
  date: string;
  attendees: number;
  userId: number;
  description: string;
  coverUrl: string;
}
