import { Component, Input, OnInit } from '@angular/core';
import { Event } from '../event.model';

@Component({
  selector: 'app-event-card',
  templateUrl: './event-card.component.html',
  styleUrls: ['./event-card.component.css'],
})
export class EventCardComponent implements OnInit {
  @Input() event?: Event;
  isGoing: boolean = false;
  constructor() {}

  ngOnInit(): void {}

  toggleGoing(): void {
    this.isGoing = !this.isGoing;
  }
}
