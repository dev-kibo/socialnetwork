import { Component, OnInit } from '@angular/core';
import { ChatService } from '../chat/services/chat.service';
import { ActivatedRoute } from '@angular/router';
import { User } from '../chat/models/user.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  user?: User;
  friends: number = 0;
  routerLinkActiveOptions = {
    exact: true,
  };

  isFriendRequestSent: boolean = false;

  constructor(
    private chatService: ChatService,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.params.subscribe((params) => {
      const id: number = Number.parseInt(params['id']);
      this.user = this.chatService.getUsers().find((x) => x.id === id);
      console.log();
    });
    this.friends = Math.floor(Math.random() * (5000 - 1) * 1);
  }

  ngOnInit(): void {}

  toggleFriendRequest(): void {
    this.isFriendRequestSent = !this.isFriendRequestSent;
  }
}
