import { Component, Input, OnInit } from '@angular/core';
import { User } from '../../chat/models/user.model';

@Component({
  selector: 'app-person-card',
  templateUrl: './person-card.component.html',
  styleUrls: ['./person-card.component.css'],
})
export class PersonCardComponent implements OnInit {
  @Input() person?: User;
  isRequestSent: boolean = false;
  sharedFriends: number;

  constructor() {
    this.sharedFriends = this.getRandomNumber();
  }

  ngOnInit(): void {}

  toggleRequest(): void {
    this.isRequestSent = !this.isRequestSent;
  }

  getRandomNumber(): number {
    return Math.floor(Math.random() * (50 - 1) + 1);
  }
}
