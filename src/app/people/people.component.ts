import { Component, OnInit } from '@angular/core';
import { ChatService } from '../chat/services/chat.service';
import { User } from '../chat/models/user.model';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css'],
})
export class PeopleComponent implements OnInit {
  people: User[];
  constructor(private chatService: ChatService) {
    this.people = chatService.getUsers();
  }

  ngOnInit(): void {}
}
