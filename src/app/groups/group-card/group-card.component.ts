import { Component, Input, OnInit } from '@angular/core';
import { Group } from '../group.model';

@Component({
  selector: 'app-group-card',
  templateUrl: './group-card.component.html',
  styleUrls: ['./group-card.component.css'],
})
export class GroupCardComponent implements OnInit {
  @Input() group?: Group;
  isJoined: boolean = false;
  constructor() {}

  ngOnInit(): void {}

  toggleJoin(): void {
    this.isJoined = !this.isJoined;
  }
}
