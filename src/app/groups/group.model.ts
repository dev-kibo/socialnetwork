export interface Group {
  id: number;
  coverUrl: string;
  name: string;
  members: number;
  description: string;
}
