import { Component, OnInit } from '@angular/core';
import { GroupsService } from './groups.service';
import { Group } from './group.model';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css'],
})
export class GroupsComponent implements OnInit {
  groups: Group[];
  constructor(private groupsService: GroupsService) {
    this.groups = groupsService.getGroups();
  }

  ngOnInit(): void {}
}
