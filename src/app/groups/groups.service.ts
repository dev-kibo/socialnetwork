import { Injectable } from '@angular/core';
import { Group } from './group.model';
import { GROUPS } from '../../data/groups.data';

@Injectable({
  providedIn: 'root',
})
export class GroupsService {
  private groups: Group[] = GROUPS;
  constructor() {}

  getGroups() {
    return this.groups;
  }
}
