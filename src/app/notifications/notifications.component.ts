import { Component, OnInit } from '@angular/core';
import { NotificationTypes } from './notification-types.enum';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css'],
})
export class NotificationsComponent implements OnInit {
  readonly NotificationTypes = NotificationTypes;
  constructor() {}

  ngOnInit(): void {}
}
