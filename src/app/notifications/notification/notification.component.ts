import { Component, Input, OnInit } from '@angular/core';
import { NotificationTypes } from '../notification-types.enum';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css'],
})
export class NotificationComponent implements OnInit {
  @Input() type: NotificationTypes = NotificationTypes.Activity;
  readonly NotificationTypes = NotificationTypes;
  constructor() {}

  ngOnInit(): void {}
}
