import { Component, Input, OnInit } from '@angular/core';
import { User } from '../../chat/models/user.model';

@Component({
  selector: 'app-friend-suggestion',
  templateUrl: './friend-suggestion.component.html',
  styleUrls: ['./friend-suggestion.component.css'],
})
export class FriendSuggestionComponent implements OnInit {
  @Input() friend?: User;
  isRequestSent: boolean = false;
  constructor() {}

  ngOnInit(): void {}

  toggleIsRequestSent(): void {
    this.isRequestSent = !this.isRequestSent;
  }
}
