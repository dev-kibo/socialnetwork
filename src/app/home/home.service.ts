import { Injectable } from '@angular/core';
import { POSTS } from 'src/data/home-posts.data';
import { Post } from './post.model';

@Injectable({
  providedIn: 'root',
})
export class HomeService {
  private posts: Post[] = POSTS;
  constructor() {}

  getPosts(): Post[] {
    return this.posts;
  }
}
