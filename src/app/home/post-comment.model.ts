export interface PostComment {
  id: number;
  firstName: string;
  lastName: string;
  content: string;
  createdAt: string;
  profileImage: string;
}
