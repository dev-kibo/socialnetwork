import { Component, OnInit } from '@angular/core';
import { HomeService } from './home.service';
import { Post } from './post.model';
import { User } from '../chat/models/user.model';
import { ChatService } from '../chat/services/chat.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  friendSuggestions: User[];
  constructor(
    private homeService: HomeService,
    private chatService: ChatService
  ) {
    this.friendSuggestions = this.chatService
      .getUsers()
      .sort(() => 0.5 - Math.random())
      .slice(0, 5);
  }

  ngOnInit(): void {}

  get posts(): Post[] {
    return this.homeService.getPosts();
  }
}
