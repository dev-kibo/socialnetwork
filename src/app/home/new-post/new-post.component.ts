import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css'],
})
export class NewPostComponent implements OnInit {
  isPostButtonVisible: boolean = false;
  constructor() {}

  ngOnInit(): void {}

  toggleIsPostButtonVisible(event: Event): void {
    const value = (event.target as HTMLInputElement).value;
    this.isPostButtonVisible = value.length > 0 ? true : false;
  }
}
