import { PostComment } from './post-comment.model';

export interface Post {
  id: number;
  firstName: string;
  lastName: string;
  createdAt: string;
  profileImage: string;
  content: string;
  imageContent?: string;
  comments: PostComment[];
}
