import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { MainComponent } from './main/main.component';
import { PeopleComponent } from './people/people.component';
import { GroupsComponent } from './groups/groups.component';
import { EventsComponent } from './events/events.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { PostsComponent } from './user-profile/posts/posts.component';
import { InformationComponent } from './user-profile/information/information.component';
import { FriendsComponent } from './user-profile/friends/friends.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { GroupComponent } from './group/group.component';
import { MembersComponent } from './group/members/members.component';
import { GroupPostsComponent } from './group/group-posts/group-posts.component';
import { EventComponent } from './event/event.component';
import { EventInformationComponent } from './event/event-information/event-information.component';
import { EventAttendeesComponent } from './event/event-attendees/event-attendees.component';
import { CreateGroupComponent } from './groups/create-group/create-group.component';
import { CreateEventComponent } from './events/create-event/create-event.component';
import { CommunitiesComponent } from './user-profile/communities/communities.component';
import { ProfileComponent } from './profile/profile.component';
import { PrivateComponent } from './profile/private/private.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        component: HomeComponent,
        pathMatch: 'full',
      },
      {
        path: 'people',
        component: PeopleComponent,
        pathMatch: 'full',
      },
      {
        path: 'groups',
        component: GroupsComponent,
        pathMatch: 'full',
      },
      {
        path: 'groups/create',
        component: CreateGroupComponent,
        pathMatch: 'full',
      },
      {
        path: 'groups/:id',
        component: GroupComponent,
        children: [
          {
            path: '',
            component: GroupPostsComponent,
          },
          {
            path: 'members',
            component: MembersComponent,
          },
        ],
      },
      {
        path: 'events',
        component: EventsComponent,
        pathMatch: 'full',
      },
      {
        path: 'events/create',
        component: CreateEventComponent,
        pathMatch: 'full',
      },
      {
        path: 'events/:id',
        component: EventComponent,
        children: [
          {
            path: '',
            component: EventInformationComponent,
          },
          {
            path: 'attendees',
            component: EventAttendeesComponent,
          },
        ],
      },
      {
        path: 'notifications',
        component: NotificationsComponent,
        pathMatch: 'full',
      },
      {
        path: 'profile/me',
        component: UserProfileComponent,
        children: [
          {
            path: '',
            component: PostsComponent,
            pathMatch: 'full',
          },
          {
            path: 'info',
            component: InformationComponent,
            pathMatch: 'full',
          },
          {
            path: 'friends',
            component: FriendsComponent,
            pathMatch: 'full',
          },
          {
            path: 'communities',
            component: CommunitiesComponent,
            pathMatch: 'full',
          },
        ],
      },
      {
        path: 'profile/:id',
        component: ProfileComponent,
        children: [
          {
            path: '',
            component: PrivateComponent,
            pathMatch: 'full',
          },
          {
            path: 'info',
            component: PrivateComponent,
            pathMatch: 'full',
          },
          {
            path: 'friends',
            component: PrivateComponent,
            pathMatch: 'full',
          },
          {
            path: 'communities',
            component: PrivateComponent,
            pathMatch: 'full',
          },
        ],
      },
    ],
  },
  {
    path: 'account/login',
    component: LoginComponent,
    pathMatch: 'full',
  },
  {
    path: 'account/register',
    component: RegisterComponent,
    pathMatch: 'full',
  },
];

// scroll not being restored when changing routes
// https://github.com/angular/angular/issues/7791
@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
