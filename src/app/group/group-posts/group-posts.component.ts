import { Component, OnInit } from '@angular/core';
import { GroupService } from '../groupService.service';
import { HomeService } from '../../home/home.service';
import { Post } from '../../home/post.model';

@Component({
  selector: 'app-group-posts',
  templateUrl: './group-posts.component.html',
  styleUrls: ['./group-posts.component.css'],
})
export class GroupPostsComponent implements OnInit {
  isJoined: boolean = false;
  constructor(
    private groupService: GroupService,
    private homeService: HomeService
  ) {
    this.groupService.isJoined.subscribe((x) => (this.isJoined = x));
  }

  ngOnInit(): void {}

  get posts(): Post[] {
    return this.homeService.getPosts();
  }
}
