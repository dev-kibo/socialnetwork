import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GroupService {
  private isJoinedSource = new BehaviorSubject<boolean>(false);
  isJoined = this.isJoinedSource.asObservable();

  toggleJoin(): void {
    this.isJoinedSource.next(!this.isJoinedSource.getValue());
  }
}
