import { Component, OnInit } from '@angular/core';
import { ChatService } from '../../chat/services/chat.service';
import { User } from '../../chat/models/user.model';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css'],
})
export class MembersComponent implements OnInit {
  constructor(private chatService: ChatService) {}

  ngOnInit(): void {}

  get members(): User[] {
    return this.chatService.getUsers();
  }
}
