import { Component, Input, OnInit } from '@angular/core';
import { User } from '../../../chat/models/user.model';

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.css'],
})
export class MemberComponent implements OnInit {
  @Input() member?: User;
  isRequestSent: boolean = false;
  constructor() {}

  ngOnInit(): void {}

  toggleFriendRequest(): void {
    this.isRequestSent = !this.isRequestSent;
  }
}
