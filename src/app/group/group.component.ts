import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GroupService } from './groupService.service';
import { Group } from '../groups/group.model';
import { GroupsService } from '../groups/groups.service';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css'],
})
export class GroupComponent implements OnInit {
  isJoined: boolean = false;
  group?: Group;
  constructor(
    private groupService: GroupService,
    private activatedRoute: ActivatedRoute,
    private groupsService: GroupsService
  ) {
    this.activatedRoute.params.subscribe((params) => {
      const groupId: number = Number.parseInt(params['id']);
      this.group = this.groupsService.getGroups().find((x) => x.id === groupId);
    });
  }

  ngOnInit(): void {
    this.groupService.isJoined.subscribe((x) => (this.isJoined = x));
  }

  toggleJoin(): void {
    this.groupService.toggleJoin();
  }
}
