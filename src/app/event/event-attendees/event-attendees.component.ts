import { Component, OnInit } from '@angular/core';
import { ChatService } from '../../chat/services/chat.service';
import { User } from '../../chat/models/user.model';

@Component({
  selector: 'app-event-attendees',
  templateUrl: './event-attendees.component.html',
  styleUrls: ['./event-attendees.component.css'],
})
export class EventAttendeesComponent implements OnInit {
  constructor(private chatService: ChatService) {}

  ngOnInit(): void {}

  get attendees(): User[] {
    return this.chatService.getUsers();
  }
}
