import { Component, Input, OnInit } from '@angular/core';
import { User } from '../../../chat/models/user.model';

@Component({
  selector: 'app-event-attendee',
  templateUrl: './event-attendee.component.html',
  styleUrls: ['./event-attendee.component.css'],
})
export class EventAttendeeComponent implements OnInit {
  @Input() attendee?: User;
  isRequestSent: boolean = false;
  constructor() {}

  ngOnInit(): void {}

  toggleFriendRequest(): void {
    this.isRequestSent = !this.isRequestSent;
  }
}
