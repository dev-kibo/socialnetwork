import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Event } from 'src/app/events/event.model';
import { EventsService } from 'src/app/events/events.service';
import { ChatService } from '../../chat/services/chat.service';
import { User } from '../../chat/models/user.model';

@Component({
  selector: 'app-event-information',
  templateUrl: './event-information.component.html',
  styleUrls: ['./event-information.component.css'],
})
export class EventInformationComponent implements OnInit {
  event?: Event;
  constructor(
    private eventsService: EventsService,
    private activatedRoute: ActivatedRoute,
    private chatService: ChatService
  ) {
    this.activatedRoute.params.subscribe((params) => {
      const id = params['id'];
      this.event = this.eventsService.getEvents().find((x) => x.id == id);
    });
  }

  ngOnInit(): void {}

  get user(): User | undefined {
    return this.chatService.getUsers().find((x) => x.id === this.event?.userId);
  }
}
