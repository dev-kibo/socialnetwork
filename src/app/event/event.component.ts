import { Component, OnInit } from '@angular/core';
import { EventsService } from '../events/events.service';
import { ActivatedRoute } from '@angular/router';
import { Event } from '../events/event.model';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css'],
})
export class EventComponent implements OnInit {
  isGoing: boolean = false;
  event?: Event;
  constructor(
    private eventsService: EventsService,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.params.subscribe((params) => {
      const id = params['id'];
      this.event = this.eventsService.getEvents().find((x) => x.id == id);
    });
  }

  ngOnInit(): void {}

  toggleGoing(): void {
    this.isGoing = !this.isGoing;
  }
}
