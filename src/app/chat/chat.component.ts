import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MessageDirective } from './directives/message.directive';
import { ChatService } from './services/chat.service';
import { Conversation } from './models/conversation.model';
import { NewMessageWindowComponent } from './new-message-window/new-message-window.component';
import { MessageWindowComponent } from './message-window/message-window.component';
import { ChatWindow } from './models/chat-window.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class ChatComponent implements OnInit {
  openChatWindows: ChatWindow[] = [];
  contactList: Conversation[] = [];
  isChatOpen: boolean = false;
  replacedChatWindowIndex: number = 0;

  @ViewChild(MessageDirective, { static: true }) messageHost!: MessageDirective;

  constructor(private chatService: ChatService, private router: Router) {}

  ngOnInit(): void {
    this.getContactList();
    this.reloadChatWindows();
    this.chatService.closeChatWindowId.subscribe((x) => this.onWindowClosed(x));
    this.chatService.openChatWindow.subscribe((x) => {
      this.addOrReplaceChatWindow(MessageWindowComponent, x);
      this.reloadChatWindows();
    });
  }

  toggleChat(): void {
    this.isChatOpen = !this.isChatOpen;
  }

  onWindowClosed(id: number): void {
    this.openChatWindows = this.openChatWindows.filter((x) => x.data.id !== id);
    this.reloadChatWindows();
  }

  getContactList(): void {
    this.contactList = this.chatService
      .getContactList()
      .sort(
        (a, b) =>
          new Date(b.dateTime).getTime() - new Date(a.dateTime).getTime()
      );
  }

  onSearch(event: Event): void {
    var name = (event.target as HTMLInputElement).value;
    this.contactList = this.chatService.searchContactList(name);
  }

  reloadChatWindows() {
    const viewContainerRef = this.messageHost.viewContainerRef;
    viewContainerRef.clear();

    this.openChatWindows.forEach((elem) => {
      const componentRef = viewContainerRef.createComponent<any>(elem.type);
      componentRef.instance.data = elem.data;
    });
  }

  addOrReplaceChatWindow(type: any, data: any) {
    if (this.openChatWindows.length < 3) {
      this.openChatWindows.push({ type, data });
    } else {
      this.openChatWindows[this.replacedChatWindowIndex] = { type, data };
    }
    if (this.replacedChatWindowIndex === 2) {
      this.replacedChatWindowIndex = 0;
    } else {
      this.replacedChatWindowIndex++;
    }
  }

  openNewMessageChatWindow(): void {
    this.addOrReplaceChatWindow(NewMessageWindowComponent, {
      id: this.getRandomIntInclusive(),
    });
    this.reloadChatWindows();
  }

  openChatWindow(id: number): void {
    const data = this.chatService.getContact(id);

    if (this.openChatWindows.find((x) => x.data.id === data?.id)) {
      return;
    }

    this.addOrReplaceChatWindow(MessageWindowComponent, data);
    this.reloadChatWindows();
  }

  getRandomIntInclusive() {
    return Math.floor(Math.random() * (100000000 - 1000 + 1) + 1000);
  }

  hasRoute(route: string): boolean {
    return this.router.url.includes(route);
  }
}
