import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ChatService } from '../services/chat.service';
import { Conversation } from '../models/conversation.model';
import { User } from '../models/user.model';

@Component({
  selector: 'app-message-window',
  templateUrl: './message-window.component.html',
  styleUrls: ['./message-window.component.css'],
})
export class MessageWindowComponent implements OnInit {
  @Input() data: Conversation;
  user?: User;
  isChatOpen: boolean = true;

  constructor(private chatService: ChatService) {
    this.data = {
      dateTime: '',
      id: 0,
      lastMessage: '',
      userId: 0,
    };
  }

  ngOnInit(): void {
    this.user = this.chatService.getUsers().find((x) => x.id === this.data.id);
  }

  toggleChat(): void {
    this.isChatOpen = !this.isChatOpen;
  }

  onClose(): void {
    this.chatService.closeChatWindow(this.data.id);
  }
}
