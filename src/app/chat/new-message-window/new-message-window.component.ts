import { Component, Input, OnInit } from '@angular/core';
import { ChatService } from '../services/chat.service';
import { User } from '../models/user.model';

@Component({
  selector: 'app-new-message-window',
  templateUrl: './new-message-window.component.html',
  styleUrls: ['./new-message-window.component.css'],
})
export class NewMessageWindowComponent implements OnInit {
  @Input() data: any;
  isChatOpen: boolean = true;
  isWindowClosed: boolean = false;
  results: User[] = [];

  constructor(private chatService: ChatService) {}

  ngOnInit(): void {}

  toggleChat(): void {
    this.isChatOpen = !this.isChatOpen;
  }

  closeWindow() {
    this.chatService.closeChatWindow(this.data.id);
  }

  searchUsers(event: Event): void {
    var name = (event.target as HTMLInputElement).value;
    this.results = this.chatService.searchUsers(name);
  }

  openConversation(user: User): void {
    this.chatService.replaceChatWindow.next({
      replaceId: this.data.id,
      user,
    });
  }
}
