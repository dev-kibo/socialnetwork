import { User } from './user.model';
export interface ReplaceWindow {
  replaceId: number;
  user: User;
}
