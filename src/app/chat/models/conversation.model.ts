export interface Conversation {
  id: number;
  userId: number;
  lastMessage: string;
  dateTime: string;
}
