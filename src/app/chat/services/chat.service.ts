import { Injectable } from '@angular/core';
import { Conversation } from '../models/conversation.model';
import { CONTACTS } from '../../../data/conversations.data';
import { Subject } from 'rxjs';
import { User } from '../models/user.model';
import { USERS } from '../../../data/users.data';
import { ReplaceWindow } from '../models/replace-window.model';

@Injectable({
  providedIn: 'root',
})
export class ChatService {
  private closeChatWindowIdSource = new Subject<number>();
  private openChatWindowSource = new Subject<User>();
  replaceChatWindow = new Subject<ReplaceWindow>();
  closeChatWindowId = this.closeChatWindowIdSource.asObservable();
  openChatWindow = this.openChatWindowSource.asObservable();

  constructor() {
    this.replaceChatWindow
      .asObservable()
      .subscribe((x) => this.createConversation(x));
  }

  getContactList(): Conversation[] {
    return CONTACTS;
  }

  getUsers(): User[] {
    return USERS;
  }

  getContact(id: number): Conversation | undefined {
    return this.getContactList().find((x) => x.id === id);
  }

  searchContactList(name: string): Conversation[] {
    if (name.length === 0) {
      return this.getContactList();
    }

    name = name.toLowerCase();
    return (
      this.getContactList().filter((x) => {
        const user = this.getUsers().find((user) => user.id === x.userId);
        return (
          user?.firstName.toLowerCase().startsWith(name) ||
          user?.lastName.toLowerCase().startsWith(name)
        );
      }) ?? []
    );
  }

  closeChatWindow(id: number) {
    this.closeChatWindowIdSource.next(id);
  }

  createConversation(replaceWindow: ReplaceWindow) {
    this.closeChatWindow(replaceWindow.replaceId);
    this.openChatWindowSource.next(replaceWindow.user);
  }

  searchUsers(name: string): User[] {
    if (name.length === 0) {
      return this.getUsers();
    }

    name = name.toLowerCase();
    return this.getUsers().filter(
      (x) =>
        x.firstName.toLowerCase().startsWith(name) ||
        x.lastName.toLowerCase().startsWith(name)
    );
  }
}
