import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Conversation } from '../models/conversation.model';
import { User } from '../models/user.model';
import { ChatService } from '../services/chat.service';

@Component({
  selector: '[app-chat-contact]', // use as attribute because divide-y can't be applied otherwise
  templateUrl: './chat-contact.component.html',
  styleUrls: ['./chat-contact.component.css'],
})
export class ChatContact implements OnInit {
  @Input() conversation?: Conversation;
  user?: User;

  constructor(private chatService: ChatService) {}

  ngOnInit(): void {
    this.user = this.chatService
      .getUsers()
      .find((x) => x.id === this.conversation?.userId);
  }
}
