import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-comment',
  templateUrl: './new-comment.component.html',
  styleUrls: ['./new-comment.component.css'],
})
export class NewCommentComponent implements OnInit {
  isCommentButtonVisible: boolean = false;
  constructor() {}

  ngOnInit(): void {}

  toggleIsCommentButtonVisible(event: Event): void {
    const value = (event.target as HTMLInputElement).value;
    this.isCommentButtonVisible = value.length > 0 ? true : false;
  }
}
