import { Component, Input, OnInit } from '@angular/core';
import { Post } from '../home/post.model';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
})
export class TextPostComponent implements OnInit {
  @Input() post?: Post;
  isLiked: boolean = false;
  isModalOpen: boolean = false;
  modalImage?: string = '';
  isCommentBoxOpen: boolean = false;
  constructor() {}

  ngOnInit(): void {}

  openImage(image?: string) {
    this.isModalOpen = true;
    this.modalImage = image;
    document.body.style.overflow = 'hidden';
  }

  closeModal() {
    this.isModalOpen = false;
    this.modalImage = '';
    document.body.style.overflow = 'auto';
  }

  likePost() {
    this.isLiked = !this.isLiked;
  }

  toggleCommentBox() {
    this.isCommentBoxOpen = !this.isCommentBoxOpen;
  }
}
