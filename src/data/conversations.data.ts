import { Conversation } from '../app/chat/models/conversation.model';

export const CONTACTS: Conversation[] = [
  {
    id: 1,
    userId: 25,
    lastMessage: 'Vivamus vestibulum sagittis sapien.',
    dateTime: '2022-03-08T15:54:52Z',
  },
  {
    id: 2,
    userId: 44,
    lastMessage: 'Mauris sit amet eros.',
    dateTime: '2021-06-18T01:06:03Z',
  },
  {
    id: 3,
    userId: 32,
    lastMessage: 'Etiam pretium iaculis justo.',
    dateTime: '2021-11-23T15:45:19Z',
  },
  {
    id: 4,
    userId: 50,
    lastMessage:
      'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.',
    dateTime: '2022-04-01T12:17:29Z',
  },
  {
    id: 5,
    userId: 28,
    lastMessage:
      'In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt.',
    dateTime: '2021-11-01T13:00:17Z',
  },
  {
    id: 6,
    userId: 42,
    lastMessage:
      'Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.',
    dateTime: '2022-01-14T12:56:19Z',
  },
  {
    id: 7,
    userId: 2,
    lastMessage: 'Phasellus in felis. Donec semper sapien a libero.',
    dateTime: '2021-07-22T02:42:25Z',
  },
  {
    id: 8,
    userId: 5,
    lastMessage: 'Etiam faucibus cursus urna. Ut tellus.',
    dateTime: '2022-01-21T06:19:49Z',
  },
  {
    id: 9,
    userId: 19,
    lastMessage: 'Curabitur gravida nisi at nibh.',
    dateTime: '2021-12-01T17:37:12Z',
  },
  {
    id: 10,
    userId: 33,
    lastMessage: 'Suspendisse potenti. Nullam porttitor lacus at turpis.',
    dateTime: '2022-01-11T17:18:49Z',
  },
  {
    id: 11,
    userId: 37,
    lastMessage: 'In hac habitasse platea dictumst.',
    dateTime: '2022-03-29T23:55:01Z',
  },
  {
    id: 12,
    userId: 27,
    lastMessage:
      'Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo.',
    dateTime: '2021-08-23T07:08:58Z',
  },
  {
    id: 13,
    userId: 6,
    lastMessage:
      'Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.',
    dateTime: '2021-12-04T21:18:56Z',
  },
  {
    id: 14,
    userId: 35,
    lastMessage: 'In congue.',
    dateTime: '2021-12-05T16:49:12Z',
  },
  {
    id: 15,
    userId: 39,
    lastMessage:
      'Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.',
    dateTime: '2022-01-16T09:17:03Z',
  },
  {
    id: 16,
    userId: 16,
    lastMessage: 'Etiam justo. Etiam pretium iaculis justo.',
    dateTime: '2021-10-24T18:09:39Z',
  },
];
