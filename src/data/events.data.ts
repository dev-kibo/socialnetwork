import { Event } from 'src/app/events/event.model';

export const EVENTS: Event[] = [
  {
    id: 1,
    name: 'Morbi non quam nec dui luctus rutrum.',
    location: 'Kingsford, Fenyan',
    date: '2022-10-22T02:02:50Z',
    attendees: 1810,
    userId: 16,
    description:
      'Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.',
    coverUrl:
      'https://images.unsplash.com/photo-1523580494863-6f3031224c94?ixlib=rb-1.2.1&raw_url=true&q=80&fm=jpg&crop=entropy&cs=tinysrgb&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170',
  },
  {
    id: 2,
    name: 'Vivamus vestibulum sagittis sapien.',
    location: 'Moose, Flores da Cunha',
    date: '2022-09-29T18:41:33Z',
    attendees: 9391,
    userId: 28,
    description:
      'Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus.',
    coverUrl:
      'https://images.unsplash.com/photo-1549451371-64aa98a6f660?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170',
  },
  {
    id: 3,
    name: 'Nam tristique tortor eu pede.',
    location: 'Acker, Mont-Dore',
    date: '2023-02-28T22:39:03Z',
    attendees: 2876,
    userId: 2,
    description:
      'Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula.',
    coverUrl:
      'https://images.unsplash.com/photo-1478147427282-58a87a120781?ixlib=rb-1.2.1&raw_url=true&q=80&fm=jpg&crop=entropy&cs=tinysrgb&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170',
  },
  {
    id: 4,
    name: 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.',
    location: 'Hallows, Alto del Espino',
    date: '2023-02-13T17:16:10Z',
    attendees: 771,
    userId: 25,
    description:
      'Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien.',
    coverUrl:
      'https://images.unsplash.com/photo-1530023367847-a683933f4172?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687',
  },
  {
    id: 5,
    name: 'In congue.',
    location: 'Spaight, Kasakh',
    date: '2023-02-01T21:38:54Z',
    attendees: 7700,
    userId: 7,
    description:
      'Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.',
    coverUrl:
      'https://images.unsplash.com/photo-1569863959165-56dae551d4fc?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074',
  },
  {
    id: 6,
    name: 'Duis ac nibh.',
    location: 'Gale, Cisadap',
    date: '2022-06-19T12:23:09Z',
    attendees: 3841,
    userId: 25,
    description:
      'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus.',
    coverUrl:
      'https://images.unsplash.com/photo-1601601392622-5d18104a78fe?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170',
  },
  {
    id: 7,
    name: 'Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante.',
    location: 'Steensland, Recife',
    date: '2022-11-27T11:12:15Z',
    attendees: 5070,
    userId: 6,
    description:
      'Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.',
    coverUrl:
      'https://images.unsplash.com/photo-1566731372839-859e7cead0ef?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074',
  },
  {
    id: 8,
    name: 'Sed vel enim sit amet nunc viverra dapibus.',
    location: 'Anniversary, San Jose del Monte',
    date: '2023-02-16T08:45:10Z',
    attendees: 6580,
    userId: 47,
    description:
      'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.',
    coverUrl:
      'https://images.unsplash.com/photo-1597298184931-bf2a546401a6?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735',
  },
  {
    id: 9,
    name: 'In blandit ultrices enim.',
    location: 'Mendota, Åkersberga',
    date: '2022-07-18T10:27:21Z',
    attendees: 6280,
    userId: 17,
    description:
      'Nulla justo. Aliquam quis turpis eget elit sodales scelerisque.',
    coverUrl:
      'https://images.unsplash.com/photo-1588867092591-1e48bcc097ea?ixlib=rb-1.2.1&raw_url=true&q=80&fm=jpg&crop=entropy&cs=tinysrgb&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687',
  },
  {
    id: 10,
    name: 'Morbi non quam nec dui luctus rutrum.',
    location: 'Ruskin, Onsŏng',
    date: '2022-11-12T08:49:08Z',
    attendees: 8203,
    userId: 21,
    description:
      'Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim.',
    coverUrl:
      'https://images.unsplash.com/photo-1625260868938-021a2e3a600b?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170',
  },
  {
    id: 11,
    name: 'Curabitur at ipsum ac tellus semper interdum.',
    location: 'Delaware, Dengnan',
    date: '2023-03-31T07:00:36Z',
    attendees: 6492,
    userId: 5,
    description:
      'Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien.',
    coverUrl:
      'https://images.unsplash.com/photo-1642784353292-23a78b518be8?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170',
  },
];
