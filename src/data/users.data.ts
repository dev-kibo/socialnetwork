import { User } from '../app/chat/models/user.model';
export const USERS: User[] = [
  {
    id: 1,
    firstName: 'Loleta',
    lastName: 'Rispin',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=LongHairStraight&accessoriesType=Blank&hairColor=BrownDark&facialHairType=Blank&clotheType=BlazerShirt&eyeType=Default&eyebrowType=Default&mouthType=Default&skinColor=Light',
  },
  {
    id: 2,
    firstName: 'Selia',
    lastName: 'Clurow',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairShaggyMullet&accessoriesType=Prescription02&hairColor=Blue&facialHairType=MoustacheFancy&facialHairColor=BrownDark&clotheType=ShirtScoopNeck&clotheColor=Gray01&eyeType=Default&eyebrowType=AngryNatural&mouthType=ScreamOpen&skinColor=Brown',
  },
  {
    id: 3,
    firstName: 'Roseline',
    lastName: 'Cornels',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=LongHairCurvy&accessoriesType=Wayfarers&hairColor=SilverGray&facialHairType=MoustacheFancy&facialHairColor=Black&clotheType=ShirtCrewNeck&clotheColor=Blue01&eyeType=Hearts&eyebrowType=AngryNatural&mouthType=Default&skinColor=Tanned',
  },
  {
    id: 4,
    firstName: 'Naomi',
    lastName: 'Sconce',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=LongHairStraightStrand&accessoriesType=Blank&hairColor=Blue&facialHairType=MoustacheMagnum&facialHairColor=BrownDark&clotheType=BlazerShirt&clotheColor=Gray02&eyeType=Wink&eyebrowType=Angry&mouthType=Sad&skinColor=Tanned',
  },
  {
    id: 5,
    firstName: 'Pepito',
    lastName: 'Folke',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=LongHairBun&accessoriesType=Blank&hairColor=Blue&facialHairType=MoustacheMagnum&facialHairColor=Brown&clotheType=ShirtVNeck&clotheColor=Gray02&eyeType=Close&eyebrowType=Default&mouthType=ScreamOpen&skinColor=Black',
  },
  {
    id: 6,
    firstName: 'Hadleigh',
    lastName: 'Abramski',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=Hat&accessoriesType=Prescription02&hairColor=Platinum&facialHairType=Blank&facialHairColor=BlondeGolden&clotheType=ShirtScoopNeck&clotheColor=PastelOrange&eyeType=Squint&eyebrowType=UpDown&mouthType=Default&skinColor=DarkBrown',
  },
  {
    id: 7,
    firstName: 'Laureen',
    lastName: 'Altham',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairFrizzle&accessoriesType=Sunglasses&hairColor=Platinum&facialHairType=BeardMajestic&facialHairColor=Brown&clotheType=BlazerShirt&clotheColor=Blue03&eyeType=Default&eyebrowType=FlatNatural&mouthType=Smile&skinColor=Pale',
  },
  {
    id: 8,
    firstName: 'Norton',
    lastName: 'Boliver',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=LongHairBun&accessoriesType=Prescription02&hairColor=PastelPink&facialHairType=Blank&facialHairColor=BlondeGolden&clotheType=ShirtVNeck&clotheColor=PastelRed&eyeType=Wink&eyebrowType=Default&mouthType=Grimace&skinColor=DarkBrown',
  },
  {
    id: 9,
    firstName: 'Amaleta',
    lastName: 'People',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=LongHairBun&accessoriesType=Sunglasses&hairColor=Brown&facialHairType=BeardLight&facialHairColor=Black&clotheType=ShirtCrewNeck&clotheColor=Gray01&eyeType=Cry&eyebrowType=SadConcerned&mouthType=Smile&skinColor=Tanned',
  },
  {
    id: 10,
    firstName: 'Gwenore',
    lastName: 'Okenden',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=Hat&accessoriesType=Sunglasses&hairColor=Black&facialHairType=MoustacheMagnum&facialHairColor=Platinum&clotheType=GraphicShirt&clotheColor=Red&graphicType=Selena&eyeType=Dizzy&eyebrowType=UnibrowNatural&mouthType=Serious&skinColor=Brown',
  },
  {
    id: 11,
    firstName: 'Darill',
    lastName: 'Oldacre',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairShortCurly&accessoriesType=Prescription01&hairColor=Blonde&facialHairType=MoustacheMagnum&facialHairColor=Red&clotheType=BlazerSweater&clotheColor=PastelGreen&eyeType=Default&eyebrowType=UpDownNatural&mouthType=Disbelief&skinColor=Light',
  },
  {
    id: 12,
    firstName: 'Neddie',
    lastName: 'Betke',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairTheCaesar&accessoriesType=Kurt&hairColor=BrownDark&facialHairType=MoustacheFancy&facialHairColor=BrownDark&clotheType=GraphicShirt&clotheColor=Gray01&graphicType=Skull&eyeType=Default&eyebrowType=DefaultNatural&mouthType=Sad&skinColor=Tanned',
  },
  {
    id: 13,
    firstName: 'Wadsworth',
    lastName: 'Allanson',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=LongHairBun&accessoriesType=Blank&hairColor=Brown&facialHairType=BeardMajestic&facialHairColor=Auburn&clotheType=Overall&clotheColor=PastelYellow&eyeType=Close&eyebrowType=UpDownNatural&mouthType=Default&skinColor=Black',
  },
  {
    id: 14,
    firstName: 'Noach',
    lastName: 'Rudolph',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairShortFlat&accessoriesType=Round&hairColor=PastelPink&facialHairType=BeardLight&facialHairColor=Brown&clotheType=Hoodie&clotheColor=Heather&eyeType=Surprised&eyebrowType=UpDown&mouthType=Sad&skinColor=Tanned',
  },
  {
    id: 15,
    firstName: 'Rosita',
    lastName: 'Bardill',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairShortFlat&accessoriesType=Round&hairColor=BrownDark&facialHairType=MoustacheMagnum&facialHairColor=Blonde&clotheType=ShirtCrewNeck&clotheColor=PastelYellow&graphicType=SkullOutline&eyeType=Squint&eyebrowType=UpDownNatural&mouthType=Tongue&skinColor=Yellow',
  },
  {
    id: 16,
    firstName: 'Dania',
    lastName: 'de Guise',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairShortFlat&accessoriesType=Kurt&hairColor=Red&facialHairType=Blank&facialHairColor=Red&clotheType=ShirtScoopNeck&clotheColor=Gray02&eyeType=Hearts&eyebrowType=UpDown&mouthType=Serious&skinColor=DarkBrown',
  },
  {
    id: 17,
    firstName: 'Galven',
    lastName: 'Henric',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=Turban&accessoriesType=Blank&hatColor=PastelBlue&hairColor=PastelPink&facialHairType=BeardMajestic&facialHairColor=Platinum&clotheType=Hoodie&clotheColor=Gray02&graphicType=Diamond&eyeType=Side&eyebrowType=DefaultNatural&mouthType=Twinkle&skinColor=DarkBrown',
  },
  {
    id: 18,
    firstName: 'Quentin',
    lastName: 'Stonhouse',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=Turban&accessoriesType=Round&hatColor=White&hairColor=SilverGray&facialHairType=BeardLight&facialHairColor=Auburn&clotheType=ShirtScoopNeck&clotheColor=PastelOrange&graphicType=Resist&eyeType=Cry&eyebrowType=UpDownNatural&mouthType=Tongue&skinColor=DarkBrown',
  },
  {
    id: 19,
    firstName: 'Felicdad',
    lastName: 'Branford',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairShaggyMullet&accessoriesType=Round&hatColor=Gray02&hairColor=SilverGray&facialHairType=BeardLight&facialHairColor=Platinum&clotheType=ShirtVNeck&clotheColor=PastelOrange&eyeType=Wink&eyebrowType=SadConcerned&mouthType=ScreamOpen&skinColor=DarkBrown',
  },
  {
    id: 20,
    firstName: 'Ardyce',
    lastName: 'Fidoe',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=LongHairFro&accessoriesType=Sunglasses&hairColor=Auburn&facialHairType=BeardMajestic&facialHairColor=Black&clotheType=BlazerSweater&clotheColor=PastelBlue&eyeType=Wink&eyebrowType=Default&mouthType=Smile&skinColor=Pale',
  },
  {
    id: 21,
    firstName: 'Waylon',
    lastName: 'Olechnowicz',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairFrizzle&accessoriesType=Kurt&hairColor=Red&facialHairType=BeardLight&facialHairColor=BrownDark&clotheType=ShirtScoopNeck&clotheColor=PastelRed&eyeType=Close&eyebrowType=Angry&mouthType=Default&skinColor=Brown',
  },
  {
    id: 22,
    firstName: 'Horace',
    lastName: 'Leatherbarrow',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=LongHairBun&accessoriesType=Kurt&hairColor=Black&facialHairType=BeardMajestic&facialHairColor=BlondeGolden&clotheType=CollarSweater&clotheColor=Pink&eyeType=Wink&eyebrowType=DefaultNatural&mouthType=Grimace&skinColor=Pale',
  },
  {
    id: 23,
    firstName: 'Marabel',
    lastName: 'Paradine',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairShaggyMullet&accessoriesType=Wayfarers&hairColor=Brown&facialHairType=BeardMajestic&facialHairColor=Red&clotheType=Overall&clotheColor=Blue02&eyeType=Side&eyebrowType=SadConcerned&mouthType=Tongue&skinColor=Brown',
  },
  {
    id: 24,
    firstName: 'My',
    lastName: 'Corro',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairShortFlat&accessoriesType=Blank&hairColor=PastelPink&facialHairType=MoustacheFancy&facialHairColor=Platinum&clotheType=Overall&clotheColor=White&eyeType=Close&eyebrowType=FlatNatural&mouthType=Sad&skinColor=Brown',
  },
  {
    id: 25,
    firstName: 'Therese',
    lastName: 'Niblett',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=LongHairCurly&accessoriesType=Kurt&hairColor=SilverGray&facialHairType=BeardLight&facialHairColor=Auburn&clotheType=Hoodie&clotheColor=PastelYellow&eyeType=Cry&eyebrowType=AngryNatural&mouthType=Tongue&skinColor=DarkBrown',
  },
  {
    id: 26,
    firstName: 'Baryram',
    lastName: 'Veregan',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairTheCaesarSidePart&accessoriesType=Wayfarers&hairColor=BlondeGolden&facialHairType=MoustacheFancy&facialHairColor=Blonde&clotheType=ShirtVNeck&clotheColor=Gray01&eyeType=Happy&eyebrowType=SadConcerned&mouthType=Serious&skinColor=DarkBrown',
  },
  {
    id: 27,
    firstName: 'Darren',
    lastName: 'Blanpein',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=LongHairFroBand&accessoriesType=Kurt&hairColor=Black&facialHairType=Blank&facialHairColor=Brown&clotheType=ShirtCrewNeck&clotheColor=Red&eyeType=Wink&eyebrowType=AngryNatural&mouthType=Twinkle&skinColor=Black',
  },
  {
    id: 28,
    firstName: 'Trudey',
    lastName: 'Herries',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=LongHairBun&accessoriesType=Prescription01&hairColor=Black&facialHairType=BeardMedium&facialHairColor=BlondeGolden&clotheType=Overall&clotheColor=Gray01&eyeType=Dizzy&eyebrowType=RaisedExcited&mouthType=Concerned&skinColor=Tanned',
  },
  {
    id: 29,
    firstName: 'Morry',
    lastName: 'Fone',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairFrizzle&accessoriesType=Wayfarers&hairColor=Blue&facialHairType=MoustacheFancy&facialHairColor=Black&clotheType=Hoodie&clotheColor=PastelYellow&eyeType=WinkWacky&eyebrowType=UnibrowNatural&mouthType=ScreamOpen&skinColor=Brown',
  },
  {
    id: 30,
    firstName: 'Rosabella',
    lastName: 'Snar',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairShortFlat&accessoriesType=Prescription01&hairColor=BrownDark&facialHairType=MoustacheFancy&facialHairColor=Blonde&clotheType=ShirtScoopNeck&clotheColor=Black&eyeType=Default&eyebrowType=SadConcernedNatural&mouthType=ScreamOpen&skinColor=Tanned',
  },
  {
    id: 31,
    firstName: 'Wolfie',
    lastName: 'Kings',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=WinterHat2&accessoriesType=Prescription02&hatColor=Blue03&hairColor=SilverGray&facialHairType=BeardMajestic&facialHairColor=Auburn&clotheType=BlazerShirt&clotheColor=Gray02&graphicType=Skull&eyeType=Default&eyebrowType=RaisedExcited&mouthType=Eating&skinColor=Pale',
  },
  {
    id: 32,
    firstName: 'Willdon',
    lastName: 'Petticrew',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=LongHairShavedSides&accessoriesType=Blank&hatColor=Blue02&facialHairType=Blank&facialHairColor=BlondeGolden&clotheType=GraphicShirt&clotheColor=Pink&graphicType=Diamond&eyeType=EyeRoll&eyebrowType=SadConcerned&mouthType=Disbelief&skinColor=Tanned',
  },
  {
    id: 33,
    firstName: 'Linell',
    lastName: 'Abethell',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=LongHairStraightStrand&accessoriesType=Wayfarers&hairColor=Black&facialHairType=MoustacheMagnum&facialHairColor=Platinum&clotheType=GraphicShirt&clotheColor=PastelGreen&graphicType=Deer&eyeType=Cry&eyebrowType=SadConcernedNatural&mouthType=ScreamOpen&skinColor=Pale',
  },
  {
    id: 34,
    firstName: 'Susy',
    lastName: 'Ivchenko',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairShortFlat&accessoriesType=Kurt&hairColor=Blue&facialHairType=BeardLight&facialHairColor=BrownDark&clotheType=Overall&clotheColor=Gray02&graphicType=Hola&eyeType=Hearts&eyebrowType=Default&mouthType=Eating&skinColor=Light',
  },
  {
    id: 35,
    firstName: 'Klemens',
    lastName: 'Critzen',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairDreads02&accessoriesType=Prescription01&hairColor=Blue&facialHairType=BeardLight&facialHairColor=Platinum&clotheType=BlazerSweater&clotheColor=Heather&eyeType=Surprised&eyebrowType=Angry&mouthType=Disbelief&skinColor=Yellow',
  },
  {
    id: 36,
    firstName: 'Sherry',
    lastName: 'Roistone',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=LongHairStraight&accessoriesType=Prescription01&hatColor=Black&hairColor=Brown&facialHairType=Blank&facialHairColor=Brown&clotheType=Overall&clotheColor=Pink&eyeType=Wink&eyebrowType=UpDownNatural&mouthType=Default&skinColor=Pale',
  },
  {
    id: 37,
    firstName: 'Tull',
    lastName: 'Klemke',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairDreads02&accessoriesType=Prescription02&hairColor=Platinum&facialHairType=BeardMajestic&facialHairColor=Red&clotheType=BlazerSweater&clotheColor=Blue01&eyeType=Close&eyebrowType=RaisedExcitedNatural&mouthType=Default&skinColor=Pale',
  },
  {
    id: 38,
    firstName: 'Davina',
    lastName: 'Winny',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairSides&accessoriesType=Round&hairColor=BrownDark&facialHairType=Blank&facialHairColor=Blonde&clotheType=BlazerSweater&eyeType=Surprised&eyebrowType=RaisedExcitedNatural&mouthType=Concerned&skinColor=Brown',
  },
  {
    id: 39,
    firstName: 'Grantley',
    lastName: 'Huxtable',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=LongHairBun&accessoriesType=Kurt&hairColor=Brown&facialHairType=BeardMajestic&facialHairColor=BrownDark&clotheType=BlazerSweater&eyeType=EyeRoll&eyebrowType=SadConcernedNatural&mouthType=Disbelief&skinColor=Tanned',
  },
  {
    id: 40,
    firstName: 'Margery',
    lastName: 'Tarpey',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=LongHairFro&accessoriesType=Wayfarers&hairColor=Blonde&facialHairType=BeardMajestic&facialHairColor=BrownDark&clotheType=CollarSweater&clotheColor=Blue01&eyeType=Hearts&eyebrowType=RaisedExcitedNatural&mouthType=Concerned&skinColor=Tanned',
  },
  {
    id: 41,
    firstName: 'Scott',
    lastName: 'Shird',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=LongHairStraight2&accessoriesType=Blank&hairColor=Auburn&facialHairType=Blank&facialHairColor=Auburn&clotheType=BlazerShirt&clotheColor=PastelGreen&eyeType=EyeRoll&eyebrowType=UpDown&mouthType=Sad&skinColor=Black',
  },
  {
    id: 42,
    firstName: 'Christean',
    lastName: 'Winchcombe',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairTheCaesarSidePart&accessoriesType=Kurt&hairColor=Blue&facialHairType=BeardMajestic&facialHairColor=Blonde&clotheType=BlazerShirt&clotheColor=Gray02&eyeType=Close&eyebrowType=RaisedExcited&mouthType=Smile&skinColor=DarkBrown',
  },
  {
    id: 43,
    firstName: 'Karlyn',
    lastName: 'Hynam',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairTheCaesar&accessoriesType=Prescription01&hairColor=Blonde&facialHairType=BeardLight&facialHairColor=Black&clotheType=Hoodie&clotheColor=Gray01&eyeType=Happy&eyebrowType=SadConcerned&mouthType=Grimace&skinColor=Light',
  },
  {
    id: 44,
    firstName: 'Freeman',
    lastName: 'Snaden',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=WinterHat3&accessoriesType=Blank&hatColor=Blue02&hairColor=BlondeGolden&facialHairType=BeardMedium&facialHairColor=Platinum&clotheType=GraphicShirt&clotheColor=PastelBlue&graphicType=Bat&eyeType=EyeRoll&eyebrowType=FlatNatural&mouthType=Smile&skinColor=Pale',
  },
  {
    id: 45,
    firstName: 'Joellyn',
    lastName: 'Constanza',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairFrizzle&accessoriesType=Wayfarers&hatColor=Red&hairColor=Blue&facialHairType=MoustacheMagnum&facialHairColor=Auburn&clotheType=ShirtScoopNeck&clotheColor=Blue01&graphicType=Cumbia&eyeType=Side&eyebrowType=UnibrowNatural&mouthType=Default&skinColor=DarkBrown',
  },
  {
    id: 46,
    firstName: 'Davida',
    lastName: 'Town',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairShortFlat&accessoriesType=Wayfarers&hairColor=PastelPink&facialHairType=BeardMajestic&facialHairColor=Auburn&clotheType=ShirtScoopNeck&clotheColor=Pink&eyeType=Happy&eyebrowType=UpDownNatural&mouthType=ScreamOpen&skinColor=Tanned',
  },
  {
    id: 47,
    firstName: 'Turner',
    lastName: 'Wibberley',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=Hijab&accessoriesType=Wayfarers&hatColor=Blue03&hairColor=Blonde&facialHairType=BeardMajestic&facialHairColor=BrownDark&clotheType=CollarSweater&clotheColor=PastelYellow&eyeType=Cry&eyebrowType=DefaultNatural&mouthType=Smile&skinColor=DarkBrown',
  },
  {
    id: 48,
    firstName: 'Arielle',
    lastName: 'Wassung',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairShortCurly&accessoriesType=Prescription02&hatColor=White&hairColor=SilverGray&facialHairType=MoustacheFancy&facialHairColor=BrownDark&clotheType=CollarSweater&clotheColor=Black&eyeType=Wink&eyebrowType=Default&mouthType=Twinkle&skinColor=Tanned',
  },
  {
    id: 49,
    firstName: 'Ethelin',
    lastName: 'Moreno',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairSides&accessoriesType=Sunglasses&hairColor=BrownDark&facialHairType=MoustacheMagnum&facialHairColor=BrownDark&clotheType=ShirtVNeck&clotheColor=Blue03&eyeType=Surprised&eyebrowType=UpDownNatural&mouthType=Default&skinColor=Black',
  },
  {
    id: 50,
    firstName: 'Jermain',
    lastName: 'Connochie',
    profileImage:
      'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairTheCaesar&accessoriesType=Blank&hairColor=Brown&facialHairType=Blank&facialHairColor=Brown&clotheType=CollarSweater&clotheColor=PastelRed&eyeType=Close&eyebrowType=FlatNatural&mouthType=Tongue&skinColor=Pale',
  },
];
